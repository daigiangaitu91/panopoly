<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
unset($content['comments']);
unset($content['links']);

$picture_instance = field_info_instance('node', 'field_slider_collection', 'module_slider');

$entity_type = 'field_collection_item';
$bundle_name = 'field_slider_collection';

$info = field_info_instances($entity_type, $bundle_name);
$crop_style_list = $info['field_slider_image']['widget']['settings']['manualcrop_styles_list'];
ksort($crop_style_list);
$background_image = isset($field->field_slider_background_image[LANGUAGE_NONE][0]['value']) ? $field->field_slider_background_image[LANGUAGE_NONE][0]['value'] : '';

?>
<div class="slider-home slidershow style-1">
  <div data-slider data-mode-center="false" style="background-image: url(assets/images/slider.jpg);" class="inner">
    <?php
    if (isset($content['field_slider_collection']['#items'])) :
      foreach ($content['field_slider_collection']['#items'] as $key => $item) :
        $field = field_collection_item_load($item['value']);        
        $image = isset($field->field_slider_image[LANGUAGE_NONE][0]['uri']) ? $field->field_slider_image[LANGUAGE_NONE][0]['uri'] : '';
        $interchange = '';
        if ($crop_style_list) :
          $interchange = bpce_nrj_interchange_images($image, $crop_style_list, 'module_slider');
        else :
          $crop_style_list = array('slider_image', 'slider_image', 'slider_image');
          $interchange = bpce_nrj_interchange_images($image, $crop_style_list, 'module_slider');
        endif;
        $title = isset($field->field_slider_title[LANGUAGE_NONE][0]['value']) ? $field->field_slider_title[LANGUAGE_NONE][0]['value'] : '';
        $description = isset($field->field_slider_description[LANGUAGE_NONE][0]['value']) ? $field->field_slider_description[LANGUAGE_NONE][0]['value'] : '';
        $link = isset($field->field_slider_cta[LANGUAGE_NONE][0]['url']) ? $field->field_slider_cta[LANGUAGE_NONE][0]['url'] : '';
        $date = time();
        $external_url = bpce_nrj_check_url_external($link);
        ?>
        <div class="item">
          <div class="caption-block">
            <div class="inner">
              <h2 class="title">
                <?php print $title; ?>
              </h2>
              <div class="content">
                <?php if ($image): ?>              
                  <div class="col">
                    <div class="thumb">
                      <img src="<?php print $interchange['data_thumb']; ?>" alt="bpce"/>
                    </div>
                  </div>
                  <?php
                endif;
                if ($description):
                  ?>
                  <div class="col">
                    <div class="text">
                      <?php print $description; ?>
                      </p>
                      <div class="editor">
                        <ul>
                          <li><strong>De nombreux backstage</strong> pour vous faire vivre des expériences uniques</li>
                          <li><strong>Près de 2000 cadeaux</strong> à gagner toute l’année</li>
                          <li><strong>Des réductions</strong> sur vos marques préférées</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
              <?php if ($link): ?>
                <div class="text-center button">
                  <a href="<?php print $external_url['path']; ?>" title="<?php print "123"; ?>" class="btn-1 large">
                    <span class="fa fa-angle-right"></span>
                    <span class="text">aaaa</span>
                  </a>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div> 
        <?php
      endforeach;
    endif;
    ?>
  </div>
</div>
