<?php
/*
 * @file
 * node--module_article.tpl.php.
 *
 */
?>

<?php
// Row first get data
if (isset($content['field_row_first']['#items'])) :
  $item = $content['field_row_first']['#items'];
  $field_row_first = field_collection_item_load($item[0]['value']);
  $field_row_first_title = $field_row_first->field_title['und'][0]['value'];
  $field_row_first_description = $field_row_first->field_description['und'][0]['value'];
  $field_row_first_cta_url = isset($field_row_first->field_row_first_link['und'][0]['url']) ? $field_row_first->field_row_first_link['und'][0]['url'] : '';
  $field_row_first_cta_title = isset($field_row_first->field_row_first_link['und'][0]['title']) ? $field_row_first->field_row_first_link['und'][0]['title'] : '';
  $external_url = bpce_nrj_check_url_external($field_row_first_cta_url);
  $field_row_first_path = isset($field_row_first->field_row_first_background_image['und'][0]['uri']) ? $field_row_first->field_row_first_background_image['und'][0]['uri'] : '';
  $field_row_first_image = image_style_url('large', $field_row_first_path);
endif;
//end
// Row second get data
// Condition type text for row second
$type_text = isset($node->field_type_text_for_row_second['und'][0]['value']) ? $node->field_type_text_for_row_second['und'][0]['value'] : '';

switch ($type_text) {
  case '1column':
    if (isset($content['field_row_second_one_column']['#items'])) :
      $item = $content['field_row_second_one_column']['#items'];    
      $field_row_second_one_column = field_collection_item_load($item[0]['value']);      
      $field_row_second_one_column_title = isset($field_row_second_one_column->field_rso_title['und'][0]['value']) ? $field_row_second_one_column->field_rso_title['und'][0]['value'] : '';
      $field_row_second_one_column_description = isset($field_row_second_one_column->field_rso_description['und'][0]['value']) ? $field_row_second_one_column->field_rso_description['und'][0]['value'] : '';
      $field_row_second_one_column_cta_url = isset($field_row_second_one_column->field_rso_link['und'][0]['url']) ? $field_row_second_one_column->field_rso_link['und'][0]['url'] : '';
      $field_row_second_one_column_cta_title = isset($field_row_second_one_column->field_rso_link['und'][0]['title']) ? $field_row_second_one_column->field_rso_link['und'][0]['title'] : '';
      $field_row_second_one_column_external_url = bpce_nrj_check_url_external($field_row_first_cta_url);
      $field_row_second_one_column_path = isset($field_row_second_one_column->field_rso_background_image['und'][0]['uri']) ? $field_row_second_one_column->field_rso_background_image['und'][0]['uri'] : '';
      $field_row_second_one_column_image = image_style_url('large', $field_row_second_one_column_path);
    endif;
    break;
  case '2column':

    break;
  case '1column_wysy':
    $node->field_row_second_wysywig['und'][0]['value'];
    if (isset($node->field_row_second_wysywig['und'][0]['value'])) :
      $field_row_second_wysywig = $node->field_row_second_wysywig['und'][0]['value'];
    endif;
    break;
  default:
    break;
}
//end
?>
<div class="image-text banner-item spacing-bottom">
  <div class="grid-fluid">       
    <div class="row">
      <div class="col desc">
        <?php if ($field_row_first_title): ?>
          <h2 class="title"><?php print $field_row_first_title; ?></h2>
        <?php endif; ?>
        <?php if ($field_row_first_description): ?>
          <p><?php print nl2br($field_row_first_description); ?></p>
        <?php endif; ?>
        <a href="<?php print $external_url['path']; ?>" title="<?php print $field_row_first_cta_title; ?>" <?php print ($external_url['external']) ? 'target="_blank"' : ''  ?> class="btn red-btn"><?php print $field_row_first_cta_title; ?></a>
      </div>
      <?php if ($field_row_first_image): ?>
        <div class="col image">
          <img src="<?php print $field_row_first_image; ?>">
        </div>
      <?php endif; ?>
    </div>
    <div class="content">
      <?php if ($type_text == '1column'): ?>
        <div class="row">
          <div class="col desc">
            <?php if ($field_row_second_one_column_title): ?>
              <h2 class="title"><?php print $field_row_second_one_column_title; ?></h2>
            <?php endif; ?>
            <?php if ($field_row_second_one_column_description): ?>
              <p><?php print nl2br($field_row_second_one_column_description); ?></p>
            <?php endif; ?>
            <a href="<?php print $external_url['path']; ?>" title="<?php print $field_row_second_one_column_cta_title; ?>" <?php print ($external_url['external']) ? 'target="_blank"' : ''  ?> class="btn red-btn">
              <?php print $field_row_second_one_column_cta_title; ?>
            </a>
          </div>
          <?php if ($field_row_second_one_column_image): ?>
            <div class="col image">
              <img src="<?php print $field_row_second_one_column_image; ?>">
            </div>
          <?php endif; ?>
        </div>
      <?php endif; ?>

      <?php if ($type_text == '1column_wysy'): ?>
        <div class="editor">
          <?php if ($field_row_second_wysywig): ?>
            <p><?php print nl2br($field_row_second_wysywig); ?></p>
          <?php endif; ?>
        </div>
      <?php endif; ?>

      <?php
      if ($type_text == '2column'):
        ?>
        <div class="editor two-col border">
          <div class="row">
            <?php
            if (isset($content['field_row_second']['#items'])) :
              foreach ($content['field_row_second']['#items'] as $key => $item) :
                $field_row_second = field_collection_item_load($item['value']);           
                $field_row_second_title = isset($field_row_second->field_row_second_title['und'][0]['value']) ? $field_row_second->field_row_second_title['und'][0]['value']:'';
                $field_row_second_description = isset($field_row_second->field_row_second_description['und'][0]['value']) ? $field_row_second->field_row_second_description['und'][0]['value']:'';
                
                $field_row_second_cta_url = isset($field_row_second->field_row_second_link['und'][0]['url']) ? $field_row_second->field_row_second_link['und'][0]['url'] : '';
                $field_row_second_cta_title = isset($field_row_second->field_row_second_link['und'][0]['title']) ? $field_row_second->field_row_second_link['und'][0]['title'] : '';
                $external_url = bpce_nrj_check_url_external($field_row_first_cta_url);
                $field_row_second_path = isset($field_row_second->field_row_second_background_image['und'][0]['uri']) ? $field_row_second->field_row_first_background_image['und'][0]['uri'] : '';
                $field_row_second_image = image_style_url('large', $field_row_second_path);
                ?>
                <div class="col">
                  <div class="row">
                    <div class="col desc">
                      <?php if ($field_row_second_title): ?>
                        <h2 class="title"><?php print $field_row_second_title; ?></h2>
                      <?php endif; ?>
                      <?php if ($field_row_second_description): print($field_row_second_description);?>
                        <p><?php print nl2br($field_row_second_description); ?></p>
                      <?php endif; ?>
                      <a href="<?php print $external_url['path']; ?>" title="<?php print $field_row_second_cta_title; ?>" <?php print ($external_url['external']) ? 'target="_blank"' : ''  ?> class="btn red-btn"><?php print $field_row_second_cta_title; ?></a>
                    </div>
                    <?php if ($field_row_second_image): ?>
                      <div class="col image">
                        <img src="<?php print $field_row_second_image; ?>">
                      </div>
                    <?php endif; ?>
                  </div>
                </div>
              <?php endforeach;
            endif;
            ?>      
          </div>
        </div>
        <?php
      endif;
      ?>     
    </div>
  </div>
</div>