<?php
/**
 * @file page--front.tpl.php.
 *
 */
global $language, $base_url;
$path = $base_url . '/' . path_to_theme();
?>
<div id="wrapper">
  <header class="main-header">
    <div class="grid-fluid">
      <div class="hidden-xs">
        <?php if ($logo_header_desktop): ?>
          <a class="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print file_create_url($logo_header_desktop->uri); ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>
        <?php
//        if ($page['header_main_menu']) :
//          print render($page['header_main_menu']);
//        endif;
        ?>
        <div class="tool-block">
          <?php
          if ($page['header_login']):
            print render($page['header_login']);
          endif;
          ?>
        </div>

        <?php
        if ($page['header_anchors']):
          print render($page['header_anchors']);
        endif;
        ?>
      </div>

      <?php if ($logo_header_mobile): ?>
        <div class="hidden-sm">
          <a class="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print file_create_url($logo_header_mobile->uri); ?>" alt="<?php print t('Home'); ?>"/>
          </a>
          <button type="button" name="menu-btn" id="menu-btn" title="" class="icon icon-burger" data-trigger-popup="popup-mobile">
            <span class="line"></span>
          </button>
        </div>
      <?php endif; ?>
      <?php
      if ($page['search']):
        print render($page['search']);
      endif;
      ?>
    </div>
  </header>
  <main>
    <?php if ($messages): ?>
      <div class="drupal-messages">
        <?php print $messages; ?>
      </div>
    <?php endif; ?>
    <?php print render($page['content']); ?>
  </main>

  <footer class="main-footer">
    <!-- region footer bottom -->
    <div class="footer-bottom">
      <div class="grid-fluid">
        <div class="mention-health">
          <h2 class="footer-title">
            <?php //print v2_age_gate_variable_get('mention_health', $language->language);  ?> </h2>
          <?php if ($language->language == 'fr-fr') : ?>
            <a href="http://www.consignesdetri.fr" title="www.consignesdetri.fr" class="recycling-instructions" target="_blank">
              <span>
                <?php print t('Our packages are subject of a set tri for more information: www.consignesdetri.fr'); ?>
              </span>
            </a>
          <?php endif; ?>
        </div>
        <div class="sitemap-block">
          <?php if ($logo_footer_desktop || $logo_footer_mobile): ?>
            <div class="logo-footer">
              <a class="logo hidden-xs" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
                <img src="<?php print file_create_url($logo_footer_desktop->uri); ?>" alt="<?php print t('Home'); ?>" />
              </a>
              <a class="logo hidden-sm visible-xs" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
                <img src="<?php print file_create_url($logo_footer_mobile->uri); ?>" alt="<?php print t('Home'); ?>" />
              </a>

            </div>

          <?php endif; ?>
          <!-- region sitemap -->
          <?php
          if ($page['footer_sitemap']) :
            print render($page['footer_sitemap']);
          endif;
          ?>
          <!-- end -->
        </div>
        <!-- region country -->
        <?php
        if ($page['footer_country']) :
          print render($page['footer_country']);
        endif;
        ?>
        <!-- end -->
        <!-- region menu footer -->
        <?php
        if ($page['footer_menu']) :
          print render($page['footer_menu']);
        endif;
        ?>
        <!-- end -->
      </div>
    </div>
  </footer>

</div>

<div class="loading">
  <div class="inner"><img src="<?php print $path; ?>/images/loading-icon.gif" alt="<?php print t('loading'); ?>">
  </div>
</div>
<!-- region menu footer -->
<?php
if ($page['popup']) :
  print render($page['popup']);
endif;
?>
<!-- end -->