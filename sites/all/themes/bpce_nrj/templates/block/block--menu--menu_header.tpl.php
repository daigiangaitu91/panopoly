<?php

/**
 * @file block--menu--menu_header.tpl.php.
 * Render block menu--menu header
 *
 */
?>


  <?php print render($title_prefix); ?>
  
  
  <?php print $content ?>


  <div <?php print $content_attributes; ?>>
  
  </div>

<?php print render($title_suffix); ?>
