<?php
/**
 * @file block--menu.tpl.php.
 * Render block menu
 *
 */
?>
<?php
if ($classes) {
  $classes = ' class="' . $classes . ' "';
}
?>

<nav <?php print $id_block . $classes . $attributes; ?> role="navigation">


<?php print render($title_prefix); ?>
<?php if ($block->subject): ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php print $content ?>

</nav>
