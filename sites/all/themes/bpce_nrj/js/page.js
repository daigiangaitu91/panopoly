(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * @name Site
 * @description Define global variables and functions
 * @version 1.0
 */
window.Site = (function($, window, undefined) {
  var doc = $(document),
      body = $('body');

  var GlobalEvents = {
    AJAX_SUCCESS: 'load-ajax-success'
  };

  function dayInMonthValidator() {
    window.ParsleyValidator.addValidator('numdayinmonth', function(value, requirements) {
      var max, month, year;
      if (value > 31) {
        return true;
      }
      month = $(requirements[0]);
      month = month.val().length ? parseInt(month.val()) : 0;
      year = $(requirements[1]);
      year = year.val().length ? parseInt(year.val()) : 0;
      max = 31;
      if (month !== 0 && (month === 4 || month === 6 || month === 9 || month === 11)) {
        max = 30;
      } else if (month !== 0 && month === 2) {
        if (year !== 0 && ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0)) {
          max = 29;
        } else {
          max = 28;
        }
      }
      return value <= max;
    }, 32);
  };

  function validateEmail() {
    window.ParsleyValidator.addValidator('validateemail', function(value, requirements) {
      var regEmail;
      regEmail = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/i;
      return regEmail.test(value);
    }, 32);
  };

  function checkWidthWindow() {
    if (window.Modernizr.touch) {
      return $(window).width();
    }
    else {
      if (navigator.userAgent.match(/safari/i) && !navigator.userAgent.match(/chrome/i)) {
        return document.documentElement.clientWidth;
      }
      else {
        return window.innerWidth || document.documentElement.clientWidth;
      }
    }
  }

  function isMobile() {
    return window.Modernizr.mq('(max-width: 991px)');
  }

  var freezePage = function() {
    body.addClass('freeze');
  }

  var unfreezePage = function() {
    var blocks = $('[data-popup]:visible, .overlay:visible');
    if (0 === blocks.length) {
      body.removeClass('freeze');
    }
  }

  function correctSliderPos() {
    var touchedSlide = null;
    doc.on('touchstart.setTouchedSlide', '.slick-initialized', function() {
      touchedSlide = $(this);
    })
    .on('touchend.correctSliderPos', function(e) {
      var target = $(e.target),
          isClickNextPrev = target.hasClass('slick-next') || target.hasClass('slick-prev'),
          isClickPaging = target.closest('.slick-dots').length;
      if(touchedSlide && !isClickNextPrev && !isClickPaging) {
        var slider = touchedSlide[0];
        touchedSlide = null;
        slider.slick.goTo(slider.slick.slickCurrentSlide());
      }
    });
  }

  return {
    correctSliderPos: correctSliderPos,
    getWidthWindow: checkWidthWindow,
    isMobile: isMobile,
    freezePage: freezePage,
    unfreezePage: unfreezePage,
    events: GlobalEvents,
    dayInMonthValidator: dayInMonthValidator,
    validateEmail: validateEmail
  };



})(jQuery, window);

jQuery(function($) {
  window.Site.correctSliderPos();
  window.Site.dayInMonthValidator();
  window.Site.validateEmail();
});

},{}]},{},[1]);
