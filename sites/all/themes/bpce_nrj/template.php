<?php

/*
 * @file
 * template.php
 */

function bpce_nrj_preprocess_html(&$vars) {
  $arg = arg();
  // Ensure that the $vars['rdf'] variable is an object.
  if (!isset($vars['rdf']) || !is_object($vars['rdf'])) {
    $vars['rdf'] = new StdClass();
  }

  if (module_exists('rdf')) {
    $vars['doctype'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">' . "\n";
    $vars['rdf']->version = 'version="HTML+RDFa 1.1"';
    $vars['rdf']->namespaces = $vars['rdf_namespaces'];
    $vars['rdf']->profile = ' profile="' . $vars['grddl_profile'] . '"';
  }
  else {
    $vars['doctype'] = '<!DOCTYPE html>' . "\n";
    $vars['rdf']->version = '';
    $vars['rdf']->namespaces = '';
    $vars['rdf']->profile = '';
  }

}

function bpce_nrj_config_meta_tag($param) {
  global $language;
  $list_meta = variable_get('list_meta', array());
  $open_graph_protocol = array();
  if ($list_meta[$language->language]['title_' . $param]) {
    $open_graph_protocol['title'] = $list_meta[$language->language]['title_' . $param];
  }

  if ($list_meta[$language->language]['desc_' . $param]) {
    $open_graph_protocol['description'] = $list_meta[$language->language]['desc_' . $param];
  }
  $vars['head_title'] = $open_graph_protocol['title'] . ' | ' . $vars['head_title_array']['name'];
  bpce_nrj_add_meta_tags($open_graph_protocol);
}

/**
 * Add meta tag for new page.
 */
function bpce_nrj_add_meta_tags($open_graph_protocol) {
  $elements = array();
  $elements[] = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'description',
      'content' => htmlspecialchars_decode($open_graph_protocol['description'], ENT_QUOTES),
    ),
  );

  $i = 0;
  foreach ($elements as $element) {
    drupal_add_html_head($element, 'open_graph_protocol_' . $i++);
  }
}

/**
 * Get meta tag with language default.
 */
function bpce_nrj_get_meta_tag($arg = '', $name = '') {
  global $language;
  $title = t('Mumm International');
  $list_meta = variable_get('list_meta', array());
  if ($list_meta[$language->language]['title_' . $arg]) {
    $title = $list_meta[$language->language]['title_' . $arg] . ' | ' . $name;
  }
  return $title;
}

/**
 * Implement hook_preprocess_node().
 */
function bpce_nrj_preprocess_node(&$variables) {
    $js = $css = array();

  switch ($variables['type']) {
    case 'lucky_draw':
      $js = array_merge($js, array(
        'lucky-draw',
      ));
      $css = array_merge($css, array(
        'lucky-draw'
      ));
      break;

    default:
      break;
  }
  bpce_nrj_include_asset($css, 'css');
  bpce_nrj_include_common_js($js, 'js');

  if ($variables['type'] == 'product_champagne') {
    $variables['theme_hook_suggestions'] = array('node__product_champagne__' . $variables['view_mode']);
  }

  if ($variables['type'] == 'article') {
    $variables['theme_hook_suggestions'] = array('node__article__' . $variables['view_mode']);
  }

  if ($variables['type'] == 'modular_page') {
    $variables['theme_hook_suggestions'] = array('node__modular_page__' . $variables['view_mode']);
  }
  if ($variables['type'] == 'basic_page') {
    $variables['theme_hook_suggestions'] = array('node__basic_page__' . $variables['view_mode']);
  }
  
}

function bpce_nrj_preprocess_page(&$variable) {
  drupal_add_js('http://maps.googleapis.com/maps/api/js', 'external');
  $common_css = array(
    'style',
  );
  $common_js = array(
    'vendors.min',
    'l10n.min',
    'script.min',
    //'script-age-gate.min'
  );
  bpce_nrj_include_asset($common_css, 'css');
  bpce_nrj_include_common_js($common_js, 'js');
  // Get logo for header and footer.
  $logo_arr = array('logo_header_desktop', 'logo_header_mobile', 'logo_footer_desktop', 'logo_footer_mobile');
  foreach ($logo_arr as $value) {
    $file = file_load(variable_get($value, 0));
    if ($file) {
      $variable[$value] = $file;
    }
  }

  
drupal_add_js(drupal_get_path('module', 'bpce_nrj') . '/js/script_gmap.js');
  if ($_GET['q'] === 'outdated-browser') {
    $css = drupal_add_css();
    unset($css['sites/all/themes/bpce_nrj/css/style.css']);
    bpce_nrj_include_asset($common_css, 'css');
  }
  
}

function bpce_nrj_css_alter(&$css) {
  //Remove style.css in page outdated-browser
  $path = drupal_get_path('theme', 'bpce_nrj');
  if ($_GET['q'] === 'outdated-browser') {
    unset($css[$path . '/css/style.css']);
  }
  else {
    unset($css[$path . '/css/unsupported-browsers.css']);
  }
}


function bpce_nrj_preprocess_block(&$vars, $hook) {

  $block = $vars['elements']['#block']->region . '-' .
      $vars['elements']['#block']->module . '-' .
      $vars['elements']['#block']->delta;

  switch ($block) {
    case 'footer_menu-menu-menu-doormat':
      $vars['classes_array'][] = 'menu-footer';
      break;
    case 'header_main_menu-menu-menu-new-main-menu':
      $vars['classes_array'][] = 'main-menu-desktop';
      break;
    case 'sidebar-system-navigation':
      $vars['classes_array'][] = '';
      break;
    default:

      break;
  }

  switch ($vars['elements']['#block']->region) {
    case 'header':
      $vars['classes_array'][] = '';
      break;
    case 'sidebar':
      $vars['classes_array'][] = '';
      $vars['classes_array'][] = '';
      break;
    default:

      break;
  }
}

function bpce_nrj_interchange_images($uri, $style_names = array(), $content_type = null) {

  $result = array();
  $data_thumb = '';
  // Patterns: ["http://example1.jpg","http://example2.jpg"].
  if ($style_names) {
    $key = 0;
    foreach ($style_names as $style_name) {
      $result[$key] = image_style_url($style_name, $uri);
      if ($key == 0) {
        // Condtition content type module slider.
        if ($content_type == 'module_slider') {
          unset($result[$key]);
        }
        $data_thumb = image_style_url($style_name, $uri);
      }
      $key++;
    }
    $result = '["' . implode('","', $result) . '"]';

    // Check the condition in case get data thumb
    if ($content_type) {
      return array(
        'data_thumb' => $data_thumb,
        'style_list' => $result,
      );
    }
  }

  return $result;
}

/**
 * Implement include css/js for each page.
 */
function bpce_nrj_include_asset($variable, $type) {
  $path = drupal_get_path('theme', 'bpce_nrj');
  if ($type == 'css') {
    foreach ($variable as $key => $item) {
      drupal_add_css($path . '/css/' . $item . '.css', array(
        'group' => CSS_THEME,
        'type' => 'file',
        'media' => 'screen',
        'preprocess' => FALSE,
        'every_page' => FALSE,
        'group' => CSS_THEME,
        'weight' => $key,
      ));
    }
  }
  if ($type == 'js') {
    foreach ($variable as $key => $item) {
      drupal_add_js($path . '/js/' . $item . '.js', array(
        'type' => 'file',
        'scope' => 'footer',
        'group' => JS_THEME,
        'every_page' => FALSE,
        'weight' => $key,
      ));
    }
  }
}

/**
 * Implement include js common in footer for every page.
 */
function bpce_nrj_include_common_js($variable) {
  $path = drupal_get_path('theme', 'bpce_nrj');
  foreach ($variable as $item) {
    drupal_add_js($path . '/js/' . $item . '.js', array(
      'type' => 'file',
      'scope' => 'footer',
      'group' => JS_THEME,
      'every_page' => TRUE,
      'weight' => -1,
    ));
  }
}

/*
 * hook menu tree new main menu
 */

function bpce_nrj_menu_tree__menu_new_main_menu(&$variables) {
  return '<ul>' . $variables['tree'] . '</ul>';
}

/*
 * hook menu link menu new main menu
 */

function bpce_nrj_menu_link__menu_new_main_menu(&$variables) {
  $element = $variables ['element'];
  if (!$element ['#title']) {
    $href_array = explode('/', $element['#href']);
    if ($href_array[0] == 'node' && is_numeric($href_array[1])) {
      $node = node_load($href_array[1]);
      $element['#title'] = $node->title;
    }    
  }
  $output = l($element ['#title'], $element ['#href'], $element ['#localized_options']);
  if (in_array('first-item', $element['#localized_options']['attributes']['class'])) {
    return '<li class="first">' . $output . '</li>';
  }
  return '<li class="item">' . $output . '</li>';
}

/*
 * hook menu tree menu doormat
 */

function bpce_nrj_menu_tree__menu_doormat(&$variables) {
  return $variables['tree'];
}

/*
 * hook menu link menu doormat
 */

function bpce_nrj_menu_link__menu_doormat(&$variables) {
  $element = $variables ['element'];
  $sub_menu = '';

  if ($element ['#below']) {
    $sub_menu = drupal_render($element ['#below']);
  }

  if ($element['#change_location']) {
    $output = '<a class="hidden-xs" data-trigger-toggle="change-location" title="' . $element ['#title'] . '" href="javascript:;">' . $element ['#title'] . '</a>';
  }
  else {
    $output = l($element ['#title'], $element ['#href'], $element ['#localized_options']);
  }
  return $output . $sub_menu;
}

/*
 * hook menu tree menu header
 */

function bpce_nrj_menu_tree__menu_header(&$variables) {
  return $variables['tree'];
}

/*
 * hook menu link menu header
 */

function bpce_nrj_menu_link__menu_header(&$variables) {

  $element = $variables ['element'];
  $sub_menu = '';

  if ($element ['#below']) {
    $sub_menu = drupal_render($element ['#below']);
  }

  if (in_array('search-btn', $element['#localized_options']['attributes']['class'])) {

    $output = '<button id="search-btn" class="icon icon-search-gray search-btn" data-trigger-toggle="search-box" title="" name="search-btn" type="button">
      </button>';
  }
  else {

    $output = l($element ['#title'], $element ['#href'], $element ['#localized_options']);
  }
  return $output;
}

/**
 * hook preprocess_search_results
 * @param array $variables
 */
function bpce_nrj_preprocess_search_results(&$variables) {
  $results = $variables['results'];

  $champange = array();
  $article = array();
  foreach ($results as $key => $value) {
    if ($value['type'] == 'Champagne') {
      $champange[] = $value;
    }
    else {
      $article[] = $value;
    }
  }
  $variables['search_champagne'] = $champange;
  $variables['search_article'] = $article;
}

/**
 * Implement Hook_form_element()
 * @param type $variables
 * @return string
 */
function bpce_nrj_form_element($variables) {
  $element = &$variables ['element'];

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element ['#markup']) && !empty($element ['#id'])) {
    $attributes ['id'] = $element ['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes ['class'] = array('form-item');
  if (!empty($element ['#type'])) {
    $attributes ['class'][] = 'form-type-' . strtr($element ['#type'], '_', '-');
  }
  if (!empty($element ['#name'])) {
    $attributes ['class'][] = 'form-item-' . strtr($element ['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element ['#attributes']['disabled'])) {
    $attributes ['class'][] = 'form-disabled';
  }
  $output = "";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element ['#title'])) {
    $element ['#title_display'] = 'none';
  }
  $prefix = isset($element ['#field_prefix']) ? '<span class="field-prefix">' . $element ['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element ['#field_suffix']) ? ' <span class="field-suffix">' . $element ['#field_suffix'] . '</span>' : '';

  switch ($element ['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element ['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element ['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element ['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element ['#description'])) {
    $output .= '<div class="description">' . $element ['#description'] . "</div>\n";
  }

  $output .= "";

  return $output;
}

/**
 * Implement hook_select()
 * @param type $variables
 * @return type
 */
function bpce_nrj_select($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-select'));
  $select_name = $variables['element']['#name'];
  if ($select_name == 'country') {
    return '<select' . drupal_attributes($element['#attributes']) . '>' . bpce_nrj_form_select_options($element) . '</select>';
  }
  else {
    return '<select' . drupal_attributes($element ['#attributes']) . '>' . form_select_options($element) . '</select>';
  }
}

/**
 * Implement hook_preprocess_taxonomy_term()
 */
function bpce_nrj_preprocess_taxonomy_term(&$variables) {
  if ($variables['vocabulary_machine_name'] == 'champagne_categories') {
    $contextual_links = array(
      '#type' => 'contextual_links',
      '#contextual_links' => array(
        'taxonomy' => array('taxonomy/term', array($variables['tid'])),
      ),
    );
    $variables['title_suffix']['contextual_links'] = $contextual_links;
  }
}

/**
 * Override webform element text.
 * Hidden parent field (fieldset), set same line for all remain fields.
 * Output a form element in plain text format.
 */
function bpce_nrj_webform_element_text($variables) {
  $element = $variables['element'];
  $value = $variables['element']['#children'];

  $output = '';
  $is_group = webform_component_feature($element['#webform_component']['type'], 'group');

  // Output the element title.
  if (isset($element['#title'])) {
    if ($is_group) {
      // Hidden fieldset.
      // $output .= '--' . $element['#title'] . '--';
      $output .= '';
    }
    elseif (!in_array(drupal_substr($element['#title'], -1), array('?', ':', '!', '%', ';', '@'))) {
      $output .= $element['#title'] . ': ';
    }
    else {
      $output .= $element['#title'];
    }
  }

  // Wrap long values at 65 characters, allowing for a few fieldset indents.
  // It's common courtesy to wrap at 75 characters in e-mails.
  if ($is_group && drupal_strlen($value) > 65) {
    $value = wordwrap($value, 65, "\n");
    $lines = explode("\n", $value);
    foreach ($lines as $key => $line) {
      $lines[$key] = '' . $line;
    }
    // Remove unused break line.
    // $value = implode("\n", $lines);
  }

  // Add the value to the output. Add a newline before the response if needed.
  // Remove unused break line.
  // $output .= (strpos($value, "\n") === FALSE ? ' ' : "\n") . $value;
  $output .= $value;

  // Indent fieldsets.
  if ($is_group) {
    $lines = explode("\n", $output);
    foreach ($lines as $number => $line) {
      if (strlen($line)) {
        $lines[$number] = '' . $line;
      }
    }
    $output = implode("\n", $lines);
    // Remove unused break line.
    // $output .= "\n";
  }

  if ($output) {
    if (!$is_group) {
      $output .= "\n";
    }
  }

  return $output;
}
/**
 * Check url is external.
 * @global type $base_url
 * @global type $language
 * @param type $url
 * @return string
 */
function bpce_nrj_check_url_external($url) {
  global $base_url, $language;
  $external_url = array();
  // Condition url is front or nolink.
  if ($url == '<front>' || $url == '<nolink>') {
    $external_url['path'] = $base_url . '/' . $language->prefix;
  }
  else {
    $external = url_is_external($url);
    if ($external) {
      $external_url['path'] = $url;
      $external_url['external'] = TRUE;
    }
    else {
      $external_url['path'] = $base_url . '/' . $language->prefix . '/' . $url;
      $external_url['external'] = FALSE;
    }
  }
  return $external_url;
}
