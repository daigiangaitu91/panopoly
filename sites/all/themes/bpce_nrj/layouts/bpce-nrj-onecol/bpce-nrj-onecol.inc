<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('BPCE-nrj Single column'),
  'category' => t('BPCE-nrj Columns: 1'),
  'icon' => 'bpce-nrj-onecol.png',
  'theme' => 'panels_bpce_nrj_onecol',
  'regions' => array('middle' => t('Middle column')),
);
