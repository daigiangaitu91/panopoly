<?php

$plugin = array(
  'title' => t('Text (1 & 2 Columns)'),
  'description' => t('The text module allows the creation of free text paragraphs. It has two betting options in forms: 1 or 2 columns.'),
  'category' => t('Modules'),
  'edit form' => 'module_text_node_form',
  'render callback' => 'bpce_nrj_module_render',
  'defaults' => array()
);

/**
 * Implements hook_form().
 * This configuration form can support add/edit content in FO panels.
 */
function module_text_node_form($form, &$form_state) {
  $form = bpce_nrj_node_form($form, $form_state);
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function module_text_node_form_validate(&$form, &$form_state) {

}

/**
 * Implements hook_form_submit().
 * Assign node entity of embedded module to Panels.
 */
function module_text_node_form_submit($form, &$form_state) {
  bpce_nrj_node_form_submit($form, $form_state);
}
