<?php
/**
 * @file
 * content_type_article.features.inc
 */

/**
 * Implements hook_node_info().
 */
function content_type_article_node_info() {
  $items = array(
    'module_article' => array(
      'name' => t('Module Article'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
